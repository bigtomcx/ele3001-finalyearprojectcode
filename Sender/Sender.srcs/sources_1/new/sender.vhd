----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2020/04/19 13:49:40
-- Design Name: 
-- Module Name: sender - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sender is
 Port (signal_in_0   : in std_logic;
      signal_in_1   : in std_logic;
      signal_in_2   : in std_logic;
      signal_in_3   : in std_logic;
      signal_in_4   : in std_logic;
      signal_in_5   : in std_logic;
      signal_in_6   : in std_logic;
      signal_in_7   : in std_logic;
      signal_in_8   : in std_logic;
      signal_in_9   : in std_logic;
      signal_in_10   : in std_logic;
      signal_in_11   : in std_logic;
      signal_in_12   : in std_logic;
      signal_in_13   : in std_logic;
      signal_in_14   : in std_logic;
      signal_in_15   : in std_logic;
      output  : out std_logic_vector(3 downto 0);

        signal_ack_en   : in std_logic;
        signal_req_en   : out std_logic
 );
end sender;

architecture Behavioral of sender is

signal output0 : std_logic_vector(3 downto 0) := "0000"; 
 signal output1 : std_logic_vector(3 downto 0) := "0000";
 signal output2 : std_logic_vector(3 downto 0) := "0000";
 signal output3 : std_logic_vector(3 downto 0) := "0000";
 signal output4 : std_logic_vector(3 downto 0) := "0000";
 signal output5 : std_logic_vector(3 downto 0) := "0000";
 signal output6 : std_logic_vector(3 downto 0) := "0000";
 signal output7 : std_logic_vector(3 downto 0) := "0000";
 signal output8 : std_logic_vector(3 downto 0) := "0000";
 signal output9 : std_logic_vector(3 downto 0) := "0000";
 signal output10 : std_logic_vector(3 downto 0) := "0000";
 signal output11 : std_logic_vector(3 downto 0) := "0000";
 signal output12 : std_logic_vector(3 downto 0) := "0000";
 signal output13 : std_logic_vector(3 downto 0) := "0000";
 signal output14 : std_logic_vector(3 downto 0) := "0000";
 signal output15 : std_logic_vector(3 downto 0) := "0000";

 signal req0 : std_logic :='0';
 signal req1 : std_logic :='0';
 signal req2 : std_logic :='0';
 signal req3 : std_logic :='0';
 signal req4 : std_logic :='0';
 signal req5 : std_logic :='0';
 signal req6 : std_logic :='0';
 signal req7 : std_logic :='0';
 signal req8 : std_logic :='0';
 signal req9 : std_logic :='0';
 signal req10 : std_logic :='0';
 signal req11 : std_logic :='0';
 signal req12 : std_logic :='0';
 signal req13 : std_logic :='0';
 signal req14 : std_logic :='0';
 signal req15 : std_logic :='0';
 
 
 
begin


 process (signal_in_0)

    
   begin
      if (signal_ack_en ='0'  ) then
        if (signal_in_0' event and signal_in_0 ='1') then 
        output0 <=  "0000";
         req0 <='1'  ;
        else output0 <=  "0000";
         req0 <='0'  ;
        end if;  
       end if;  

        
   end process signal_in_0 ; 
  
--The process of input1
process (signal_in_1)
   begin
    if (signal_ack_en ='0'  ) then
    --if the ack signal is 0 and input1 has a spike, then there has a output which represents the signal's address
        if (signal_in_1' event and signal_in_1 ='1') then 
        output1 <=  "0001";
    --the request signal from sender should be 1 when there has an address data comes out.
         req1 <='1'  ;
    --if the ack signal is 0 and input1 has no spike, then there is no address data generates from this signal line. 
        else output1 <=  "0000";
         req1 <='0'  ;
        end if;
    --if the ack signal is not 0  then there is no address data comes out.
        else output1 <=  "0000";
         req1 <='0'  ;
      end if;
end process signal_in_1 ; 
   
   process (signal_in_2)
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_2' event and signal_in_2 ='1') then 
        output2 <=  "0010";
         req2 <='1'  ;
        else output2 <=  "0000";
         req2 <='0'  ;
        end if;
        else output2 <=  "0000";
         req2 <='0'  ;
      end if;
   end process signal_in_2 ; 
   
   process (signal_in_3)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_3' event and signal_in_3 ='1') then 
        output3 <=  "0011";
         req3 <='1'  ;
        else output3 <=  "0000";
         req3 <='0'  ;
        end if;
      end if;
   end process signal_in_3 ; 
   
   process (signal_in_4)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_4' event and signal_in_4 ='1') then 
        output4 <=  "0100";
         req4 <='1'  ;
        else output4 <=  "0000";
         req4 <='0'  ;
        end if;
      end if;
   end process signal_in_4 ; 
   
   process (signal_in_5)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_5' event and signal_in_5 ='1') then 
        output5 <=  "0101";
         req5 <='1'  ;
        else output5 <=  "0000";
         req5 <='0'  ;
        end if;
      end if;
   end process signal_in_5 ; 
   
   process (signal_in_6)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_6' event and signal_in_6 ='1') then 
        output6 <=  "0110";
         req6 <='1'  ;
        else output6 <=  "0000";
         req6 <='0'  ;
        end if;
      end if;
   end process signal_in_6 ; 
  
 process (signal_in_7)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_7' event and signal_in_7 ='1') then 
        output7 <=  "0111";
         req7 <='1'  ;
        else output7 <=  "0000";
         req7 <='0'  ;
        end if;
      end if;
   end process signal_in_7 ;  

 process (signal_in_8)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_8' event and signal_in_8 ='1') then 
        output8 <=  "1000";
         req8 <='1'  ;
        else output8 <=  "0000";
         req8 <='0'  ;
        end if;
      end if;
   end process signal_in_8 ;  
   
  process (signal_in_9)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_9' event and signal_in_9 ='1') then 
        output9 <=  "1001";
         req9 <='1'  ;
        else output9 <=  "0000";
         req9 <='0'  ;
        end if;
      end if;
   end process signal_in_9 ; 
   
  process (signal_in_10)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_10' event and signal_in_10 ='1') then 
        output10 <=  "1010";
         req10 <='1'  ;
        else output10 <=  "0000";
         req10 <='0'  ;
        end if;
      end if;
   end process signal_in_10 ; 
   
   process (signal_in_11)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_11' event and signal_in_11 ='1') then 
        output11 <=  "1011";
         req11 <='1'  ;
        else output11 <=  "0000";
         req11 <='0'  ;
        end if;
      end if;
   end process signal_in_11 ;  
   
     process (signal_in_12)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_12' event and signal_in_12 ='1') then 
        output12 <=  "1100";
         req12 <='1'  ;
        else output12 <=  "0000";
         req12 <='0'  ;
        end if;
      end if;
   end process signal_in_12 ; 
   
     process (signal_in_13)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_13' event and signal_in_13 ='1') then 
        output13 <=  "1101";
         req13 <='1'  ;
        else output13 <=  "0000";
         req13 <='0'  ;
        end if;
      end if;
   end process signal_in_13 ; 
   
     process (signal_in_14)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_14' event and signal_in_14 ='1') then 
        output14 <=  "1110";
         req14 <='1'  ;
        else output14 <=  "0000";
         req14 <='0'  ;
        end if;
      end if;
   end process signal_in_14 ; 
   
     process (signal_in_15)
 
   begin
    if (signal_ack_en ='0'  ) then
        if (signal_in_15' event and signal_in_15 ='1') then 
        output15 <=  "1111";
         req15 <='1'  ;
        else output15 <=  "0000";
         req15 <='0'  ;
        end if;
      end if;
   end process signal_in_15 ; 
   
 output <= output0 or output1 or output2 or output3 or output4 or output5 or output6 or output7 or output8 or output9 or output10 or output11 or output12 or output13 or output14 or output15;
 signal_req_en <= req0 or req1 or req2 or req3 or req4 or req5 or req6 or req7 or req8 or req9 or req10 or req11 or req12 or req13 or req14 or req15  ;
 
end Behavioral;
