----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2020/04/19 13:45:32
-- Design Name: 
-- Module Name: Receiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Receiver is
 Port (data_in  : in std_logic_vector(3 downto 0) := "0000";

      signal_out_0   : out std_logic := '0';
      signal_out_1   : out std_logic := '0';
      signal_out_2   : out std_logic := '0';
      signal_out_3   : out std_logic := '0';
      signal_out_4   : out std_logic := '0';
      signal_out_5   : out std_logic := '0';
      signal_out_6   : out std_logic := '0';
      signal_out_7   : out std_logic := '0';
      signal_out_8   : out std_logic := '0';
      signal_out_9   : out std_logic := '0';
      signal_out_10  : out std_logic := '0';
      signal_out_11  : out std_logic := '0';
      signal_out_12  : out std_logic := '0';
      signal_out_13  : out std_logic := '0';
      signal_out_14  : out std_logic := '0';
      signal_out_15  : out std_logic := '0';

        signal_ack   : out std_logic := '0';
        signal_req   : in std_logic
 );
end Receiver;

architecture Behavioral of Receiver is
signal ack0 : std_logic :='0';
          signal ack1 : std_logic :='0';
          signal ack2 : std_logic :='0';
          signal ack3 : std_logic :='0';
          signal ack4 : std_logic :='0';
          signal ack5 : std_logic :='0';
          signal ack6 : std_logic :='0';
          signal ack7 : std_logic :='0';
          signal ack8 : std_logic :='0';
          signal ack9 : std_logic :='0';
          signal ack10 : std_logic :='0';
          signal ack11 : std_logic :='0';
          signal ack12 : std_logic :='0';
          signal ack13 : std_logic :='0';
          signal ack14 : std_logic :='0';
          signal ack15 : std_logic :='0';


begin

process (data_in)
begin
        if (signal_req ='1'  ) then
           if data_in = "0000" then 
           signal_out_0 <='1' ;
           ack0 <='1';          
       else signal_out_0 <='0';
            ack0 <='0';
           end if;
      else if (signal_req ='0'  ) then
              signal_out_0 <='0'; 
              ack0 <='0';   
       end if;
       end if;
   end process; 

--The process when generate spikes in line 1
 process (data_in)
   begin
        if (signal_req ='1'  ) then
        --if the req signal is 0 and the address is 0001,then the output spike will be generated in this line
           if data_in = "0001" then 
              signal_out_1 <='1' ;              
              ack1 <='1';              
        --if the req signal is 0 and the address is not 0001,then the output spike will not be generated in this line
             else signal_out_1 <='0';
             ack1 <='0';
           end if;
        --if the req signal is 1,then there is not output spike will be generated 
    else  if (signal_req ='0'  ) then
           signal_out_1 <='0';   
           ack1 <='0'; 
          end if;
       end if;
 end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0010" then 
              signal_out_2 <='1' ;
              
              ack2 <='1';
              
             else signal_out_2 <='0';
             ack2 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_2 <='0'; 
           ack2 <='0';   
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0011" then 
              signal_out_3 <='1' ;
              
              ack3 <='1';
              
             else signal_out_3 <='0';
             ack3 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_3 <='0';    
           ack3 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0100" then 
              signal_out_4 <='1' ;
              
              ack4 <='1';
              
             else signal_out_4 <='0';
             ack4 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_4 <='0';    
           ack4 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0101" then 
              signal_out_5 <='1' ;
              
              ack5 <='1';
              
             else signal_out_5 <='0';
             ack5 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_5 <='0';    
           ack5 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0110" then 
              signal_out_6 <='1' ;
              
              ack6 <='1';
              
             else signal_out_6 <='0';
             ack6 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_6 <='0';  
           ack6 <='0';  
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "0111" then 
              signal_out_7 <='1' ;
              
              ack7 <='1';
              
             else signal_out_7 <='0';
             ack7 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_7 <='0';    
           ack7 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1000" then 
              signal_out_8 <='1' ;
              
              ack8 <='1';
              
             else signal_out_8 <='0';
             ack8 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_8 <='0';  
           ack8 <='0';  
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1001" then 
              signal_out_9 <='1' ;
              
              ack9 <='1';
              
             else signal_out_9 <='0';
             ack9 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_9 <='0';    
           ack9 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1010" then 
              signal_out_10 <='1' ;
              
              ack10 <='1';
              
             else signal_out_10 <='0';
             ack10 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_10 <='0';    
           ack10 <='0';
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1011" then 
              signal_out_11 <='1' ;
              
              ack11 <='1';
              
             else signal_out_11 <='0';
             ack11 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_11 <='0';  
           ack11 <='0';  
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1100" then 
              signal_out_12 <='1' ;
              
              ack12 <='1';
              
             else signal_out_12 <='0';
             ack12 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_12 <='0'; 
           ack12 <='0';
             
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1101" then 
              signal_out_13 <='1' ;
              
              ack13 <='1';
              
             else signal_out_13 <='0';
             ack13 <='0';
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_13 <='0';   
           ack13 <='0'; 
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1110" then 
              signal_out_14 <='1' ;
              
              ack14 <='1';
              
             else signal_out_14 <='0';
             ack14 <='0'; 
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_14 <='0';  
           ack14 <='0';  
         end if;
       end if;
   end process;

process (data_in)
   begin
        if (signal_req ='1'  ) then
           if data_in = "1111" then 
              signal_out_15 <='1' ;
              
              ack15 <='1';
              
             else signal_out_15 <='0';
                   ack15 <='0';    
           end if;
    else  if (signal_req ='0'  ) then
           signal_out_15 <='0'; 
           ack15 <='0';    
         end if;
       end if;
   end process;

   signal_ack <= ack0 or ack1 or ack2 or ack3 or ack4 or ack5 or ack6 or ack7 or ack8 or ack9 or ack10 or ack11 or ack12 or ack13 or ack14 or ack15;

end Behavioral;
