----------------------------------------------------------------------------------

-- Engineer: Xiao Chen
-- 
-- Create Date: 2020/03/10 
-- Design Name: Test-spike generate
-- Module Name: test_spike - Behavioral
-- Project Name: ELE3001 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity test_spike is
  Port ( clk     : in std_logic;
      signal_in  : in std_logic;
      pluse_out  : out std_logic
 );
end test_spike;

architecture Behavioral of test_spike is

   signal delay_reg1 : std_logic;
   signal delay_reg2 : std_logic;

begin
   
   process (clk,signal_in)
   begin
         delay_reg1 <= signal_in;
         delay_reg2 <= delay_reg1 after 2ns;
   end process;
   pluse_out <= (NOT(delay_reg1)) AND delay_reg2;

end Behavioral;
